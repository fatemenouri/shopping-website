import bcrypt from 'bcryptjs';
const data = {
  users: [
    {
      name: 'fateme',
      email: 'admin@example.com',
      password: bcrypt.hashSync('1234', 8),
      isAdmin: true,
    },
    {
      name: 'user',
      email: 'user@example.com',
      password: bcrypt.hashSync('1234', 8),
      isAdmin: false,
    },
  ],
  products: [
    {

      name: 'هودی سبز',
      category: 'هودی',
      image: '/images/green.jpg',
      price: 120,
      countInStock: 10,
      brand: 'h&m',
      rating: 4.5,
      numReviews: 10,
      description: 'محصول با کیفیت',
    },
    {

      name: 'هودی مشکی',
      category: 'هودی',
      image: '/images/black.jpg',
      price: 100,
      countInStock: 0,
      brand: 'h&m',
      rating: 4.0,
      numReviews: 10,
      description: 'محصول با کیفیت',
      
    },
    {

      name: 'هودی کرم',
      category: 'هودی',
      image: '/images/nude.jpg',
      price: 220,
      countInStock: 15,
      brand: 'h&m',
      rating: 4.8,
      numReviews: 17,
      description: 'محصول با کیفیت',
    },
    {

      name: 'هودی صورتی',
      category: 'هودی',
      image: '/images/pink.jpg',
      price: 78,
      countInStock: 5,
      brand: 'h&m',
      rating: 4.5,
      numReviews: 14,
      description: 'محصول با کیفیت',
    },
    {
      name: 'هودی آبی',
      category: 'هودی',
      image: '/images/blue.jpg',
      price: 65,
      countInStock: 13,
      brand: 'h&m',
      rating: 4.5,
      numReviews: 10,
      description: 'محصول با کیفیت',
    },
    {
      name: 'هودی نارنجی',
      category: 'هودی',
      image: '/images/orange.jpg',
      price: 65,
      countInStock: 13,
      brand: 'h&m',
      rating: 4.5,
      numReviews: 10,
      description: 'محصول با کیفیت',
    },
    {
      name: 'هودی سفید',
      category: 'هودی',
      image: '/images/white.jpg',
      price: 65,
      countInStock: 13,
      brand: 'h&m',
      rating: 4.5,
      numReviews: 10,
      description: 'محصول با کیفیت',
    },
   
  ],
};
export default data;