import { BrowserRouter, Link, Route } from "react-router-dom";
import ProductScreen from "./screens/ProductScreen";
import HomeScreen from "./screens/HomeScreen";
import { useDispatch, useSelector } from 'react-redux';
import CartScreen from "./screens/CartScreen";
import SigninScreen from "./screens/SigninScreen";
import { signout } from './actions/userActions';
import RegisterScreen from './screens/RegisterScreen';
import ShippingAddressScreen from './screens/ShippingAddressScreen';
import PaymentMethodScreen from './screens/PaymentMethodScreen';
import PlaceOrderScreen from './screens/PlaceOrderScreen';
import OrderScreen from './screens/OrderScreen';
import OrderHistoryScreen from './screens/OrderHistoryScreen';
import ProfileScreen from './screens/ProfileScreen';
import PrivateRoute from './components/PrivateRoute';
import ProductsScreen from './screens/ProductsScreen';
import AdminRoute from "./components/AdminRoute";
import ProductListScreen from './screens/ProductListScreen';
import ProductEditScreen from './screens/ProductEditScreen';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import PersonIcon from '@material-ui/icons/Person';
import MoveToInboxOutlinedIcon from '@material-ui/icons/MoveToInboxOutlined';
import VerifiedUserOutlinedIcon from '@material-ui/icons/VerifiedUserOutlined';
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined';
import HeadsetMicOutlinedIcon from '@material-ui/icons/HeadsetMicOutlined';
import MailOutlineOutlinedIcon from '@material-ui/icons/MailOutlineOutlined';

function App() {
  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;

  const userSignin = useSelector((state) => state.userSignin);
  const { userInfo } = userSignin;
  const dispatch = useDispatch();
  const signoutHandler = () => {
    dispatch(signout());
  };
  return (

    <BrowserRouter>
      <div className="grid-container">
        <header className="row sticky">
          
          <div>
            <Link className="brand" to="/">Fatemenouri Shop</Link>
          </div>

          <div >
            <Link to="/products" className="signin">محصولات</Link>
            {userInfo ? (
              <div className="dropdown">
                <Link to="#">
                  {userInfo.name} <i className="fa fa-caret-down"></i>{' '}
                </Link>
                <ul className="dropdown-content">
                  <li>
                    <Link to="/profile">پروفایل</Link>
                  </li>
                  <li>
                    <Link to="/orderhistory">سفارشات</Link>
                  </li>
                  <li>
                    <Link to="#signout" onClick={signoutHandler}>
                      خروج
                    </Link>
                  </li>
                </ul>
              </div>
            ) : (
                <Link to="/signin" className="signin"><PersonIcon fontSize='large' />ورود</Link>
              )}
            {userInfo && userInfo.isAdmin && (
              <div className="dropdown">
                <Link to="#admin">
                  مدیریت <i className="fa fa-caret-down"></i>
                </Link>
                <ul className="dropdown-content">
                  <li>
                    <Link to="/productlist">کالاها</Link>
                  </li>
                </ul>
              </div>
            )}
            <Link to="/cart" className="signin" >
              <ShoppingCartIcon fontSize='large' />
              {cartItems.length > 0 && (
                <span className="badge">{cartItems.length} </span>
              )}
            </Link>
          </div>

        </header>
        <main>

          <Route path="/cart/:id?" component={CartScreen}></Route>
          <Route path="/product/:id" component={ProductScreen} exact></Route>
          <Route
            path="/product/:id/edit"
            component={ProductEditScreen}
            exact
          ></Route>
          <Route path="/signin" component={SigninScreen}></Route>
          <Route path="/register" component={RegisterScreen}></Route>
          <Route path="/shipping" component={ShippingAddressScreen}></Route>
          <Route path="/payment" component={PaymentMethodScreen}></Route>
          <Route path="/placeorder" component={PlaceOrderScreen}></Route>
          <Route path="/order/:id" component={OrderScreen}></Route>
          <Route path="/orderhistory" component={OrderHistoryScreen}></Route>
          <PrivateRoute
            path="/profile"
            component={ProfileScreen}
          ></PrivateRoute>
          <AdminRoute
            path="/productlist"
            component={ProductListScreen}
          ></AdminRoute>
          <Route path="/" component={HomeScreen} exact ></Route>
          <Route path="/products" component={ProductsScreen} ></Route>
          

        </main>

        <footer >
          <div className="row center icons" style={{padding:'3rem'}}>

            <MoveToInboxOutlinedIcon style={{ fontSize: '7rem', color: '#C2C2C2' }}></MoveToInboxOutlinedIcon>
            <p> تحویل سریع</p>


            <HeadsetMicOutlinedIcon style={{ fontSize: '7rem', color: '#C2C2C2' }}></HeadsetMicOutlinedIcon>
            <p>پشتیبانی 24 ساعته</p>


            <VerifiedUserOutlinedIcon style={{ fontSize: '7rem', color: '#C2C2C2' }}></VerifiedUserOutlinedIcon>
            <p>ضمانت کالا</p>


            <LocationOnOutlinedIcon style={{ fontSize: '7rem', color: '#C2C2C2' }}></LocationOnOutlinedIcon>
            <p>پرداخت محل</p>

          </div>

          <hr />
          <div style={{textAlign:'center'}}>
             
               <div className="signin">
                 fatemenouri99@gmail.com
                 <MailOutlineOutlinedIcon fontSize="large"  />
                </div> 
                <div>
              <p> تمامی حقوق محفوظ می باشد © </p>
               </div>
          </div>
         
        </footer>

      </div>
    </BrowserRouter>
  );
}

export default App;
