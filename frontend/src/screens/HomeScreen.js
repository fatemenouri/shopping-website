import React from 'react';
import { Link } from 'react-router-dom';
import { Wave } from 'react-animated-text';



export default function HomeScreen() {

  
  return (
    <>
        <div className="headerImage">
            <div className="headerText">
            <Wave
          text="!elaS retniW"
           effect="stretch"
               effectChange={2}
               direction="left"
                 />
          </div>
          <div className="headerButton">
          <button className="hbtn" variant="contained">
           <Link to="/products" style={{fontFamily:"iransans"}}>
            شروع خرید
           </Link>
         </button>
         </div>
        </div> 
     
   
        </>
    )
}
