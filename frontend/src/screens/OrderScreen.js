import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { detailsOrder } from '../actions/orderActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';

export default function OrderScreen(props) {
  const orderId = props.match.params.id;
  const orderDetails = useSelector((state) => state.orderDetails);
  const { order, loading, error } = orderDetails;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(detailsOrder(orderId));
  }, [dispatch, orderId]);
  return loading ? (
    <LoadingBox></LoadingBox>
  ) : error ? (
    <MessageBox variant="danger">{error}</MessageBox>
  ) : (
    <div>
      <h1>سفارش {order._id}</h1>
      <div className="row top">
        <div className="col-2">
          <ul>
            <li>
              <div className="card card-body">
                <h2>هزینه ی ارسال</h2>
                <p>
                  <strong>نام:</strong> {order.shippingAddress.fullName} <br />
                  <strong>آدرس: </strong> {order.shippingAddress.address},
                  {order.shippingAddress.city},{' '}
                  {order.shippingAddress.postalCode},
                  {order.shippingAddress.country}
                </p>
                {order.isDelivered ? (
                  <MessageBox variant="success">
                    تحویل شده در {order.deliveredAt}
                  </MessageBox>
                ) : (
                  <MessageBox variant="danger">تحویل نشده</MessageBox>
                )}
              </div>
            </li>
            <li>
              <div className="card card-body">
                <h2>پرداخت</h2>
                <p>
                  <strong>شیوه ی پرداخت:</strong> {order.paymentMethod}
                </p>
                {order.isPaid ? (
                  <MessageBox variant="success">
                    پرداخت شده در تاریخ {order.paidAt}
                  </MessageBox>
                ) : (
                  <MessageBox variant="danger">پرداخت نشده</MessageBox>
                )}
              </div>
            </li>
            <li>
              <div className="card card-body">
                <h2>اقلام سفارش</h2>
                <ul>
                  {order.orderItems.map((item) => (
                    <li key={item.product}>
                      <div className="row">
                        <div>
                          <img
                            src={item.image}
                            alt={item.name}
                            className="small"
                          ></img>
                        </div>
                        <div className="min-30">
                          <Link to={`/product/${item.product}`}>
                            {item.name}
                          </Link>
                        </div>

                        <div>
                           {item.qty * item.price}000 ={item.qty}x{item.price.toFixed(3)}
                        </div>
                      </div>
                    </li>
                  ))}
                </ul>
              </div>
            </li>
          </ul>
        </div>
        <div className="col-1">
          <div className="card card-body">
            <ul>
              <li>
                <h2>خلاصه سفارش</h2>
              </li>
              <li>
                <div className="row">
                  <div>سفارشات</div>
                  <div>{order.itemsPrice.toFixed(3)}</div>
                </div>
              </li>
              <li>
                <div className="row">
                  <div>هزینه ارسال</div>
                  <div>{order.shippingPrice.toFixed(3)}</div>
                </div>
              </li>
              <li>
                <div className="row">
                  <div>
                    <strong>مبلغ کل</strong>
                  </div>
                  <div>
                    <strong>{order.totalPrice.toFixed(3)}</strong>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}