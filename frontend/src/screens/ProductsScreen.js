import React, { useEffect } from 'react';
import Product from '../components/Product';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import { useDispatch, useSelector } from 'react-redux';
import { listProducts } from '../actions/productActions';
import { listProductCategories } from '../actions/productActions';
import { useState } from 'react';
import Button from '@material-ui/core/Button';


export default function ProductsScreen() {
  const dispatch = useDispatch();
  const productList = useSelector((state) => state.productList);
  const { loading, error, products } = productList;
  useEffect(() => {
    dispatch(listProducts());
  }, [dispatch]);

  const productCategoryList = useSelector((state) => state.productCategoryList);
  const {
    loading: loadingCategories,
    error: errorCategories,
    categories,
  } = productCategoryList;
  useEffect(() => {
    dispatch(listProductCategories());
  }, [dispatch]);
  const [categoryName, setCategoryName] = useState('هودی');
  return (
    <div>
      <div className="row center">
        <p>دسته بندی ها :</p>
        {loadingCategories ? (
          <LoadingBox></LoadingBox>
        ) : errorCategories ? (
          <MessageBox variant="danger">{errorCategories}</MessageBox>
        ) : (
              categories.map((c) => (
               
                  <button key={c} 
                  className=" center hbtn"
                    variant="contained"
                    onClick={() => setCategoryName(c)}
                  >
                    {c}
                  </button>
               
              ))
            )}
      </div>
      {loading ? (
        <LoadingBox></LoadingBox>
      ) : error ? (
        <MessageBox variant="danger">{error}</MessageBox>
      ) : (
            <>
              <div className="row center col-3">
                {products.map((product) =>
                  product.category === categoryName ?
                    (
                      <Product key={product._id} product={product}></Product>
                    ) : null)}
              </div>

            </>
          )}

    </div>

  );
}
