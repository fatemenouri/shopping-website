import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { savePaymentMethod } from '../actions/cartActions';
import CheckoutSteps from '../components/CheckoutSteps';

export default function PaymentMethodScreen(props) {
  const cart = useSelector((state) => state.cart);
  const { shippingAddress } = cart;
  if (!shippingAddress.address) {
    props.history.push('/shipping');
  }
  const [paymentMethod, setPaymentMethod] = useState('آنلاین');
  const dispatch = useDispatch();
  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(savePaymentMethod(paymentMethod));
    props.history.push('/placeorder');
  };
  return (
    <div>
      <CheckoutSteps step1 step2 step3></CheckoutSteps>
      <form className="form" onSubmit={submitHandler}>
        <div>
          <h1>شیوه ی پرداخت</h1>
        </div>
        <div>
          <div>
            <input
              type="radio"
              id="آنلاین"
              value="آنلاین"
              name="paymentMethod"
              required
              checked
              onChange={(e) => setPaymentMethod(e.target.value)}
            ></input>
            <label htmlFor="آنلاین">
              آنلاین</label>
          </div>
        </div>
        <div>
          <div>
            <input
              type="radio"
              id="حضوری"
              value="حضوری"
              name="paymentMethod"
              required
              onChange={(e) => setPaymentMethod(e.target.value)}
            ></input>
            <label htmlFor="حضوری">
              حضوری</label>
          </div>
        </div>
        <div>
          <label />
          <button className="primary" type="submit">
            ادامه
          </button>
        </div>
      </form>
    </div>
  );
}